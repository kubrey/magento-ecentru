<?php
/**
 * Created by PhpStorm.
 * User: kubrey
 * Date: 27.08.15
 * Time: 10:44
 */

class Kubrey_Ecentru_Block_Info_Privacy extends Mage_Core_Block_Template
{
    protected function _construct() {
        parent::_construct();
        $this->setTemplate('ecentru/info/privacy.phtml');
    }
}