<?php

/**
 * Created by PhpStorm.
 * User: kubrey
 * Date: 27.08.15
 * Time: 10:13
 */
class Kubrey_Ecentru_Block_Info_Conditions extends Mage_Core_Block_Template
{
    protected function _construct() {
        parent::_construct();
        $this->setTemplate('ecentru/info/conditions.phtml');
    }
}