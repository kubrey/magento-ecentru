<?php

class Kubrey_Ecentru_Block_Payment_Info extends Mage_Core_Block_Template
{
    protected function _construct() {
        parent::_construct();
        $this->setTemplate('ecentru/payment/info.phtml');
    }

    /**
     * @return string
     */
    public function getMethodCode() {
        return $this->getModel()->getCode();
    }

    /**
     *
     * @return Kubrey_Ecentru_Model_Ecentru
     */
    protected function getModel() {
        return Mage::getModel('ecentru/ecentru');
    }

    public function getConditions(){
        $block = $this->getLayout()
            ->createBlock('ecentru/info_conditions');
        return $block->toHtml();
    }
}