<?php

/**
 * Created by PhpStorm.
 * User: kubrey
 * Date: 27.08.15
 * Time: 13:13
 */
class Kubrey_Ecentru_Helper_Data extends Mage_Core_Helper_Abstract
{

    private $_logLevel = Zend_Log::DEBUG; //уровень, выше которого логи не записываются,соответствует Zend_Log::... от 0 до 7, 0 - наибольшая важность

    /**
     *
     * @param string $msg
     * @param int $level
     */
    public function log($msg, $level = Zend_Log::DEBUG) {
        $model = Mage::getModel('ecentru/ecentru');
        $logFile = $model->getCode() . date('Ym') . '.log';
        if ($this->_logLevel >= $level) {
            Mage::log($msg, $level, $logFile);
        }
    }
}