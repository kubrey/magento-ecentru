<?php

/**
 * Created by PhpStorm.
 * User: kubrey
 * Date: 27.08.15
 * Time: 13:17
 */
class Kubrey_Ecentru_GatewayController extends Mage_Core_Controller_Front_Action
{
    protected $_model;

    /**
     *
     * @return \Kubrey_Ecentru_Model_Ecentru
     */
    protected function getModel() {
        if (!$this->_model) {
            $this->_model = Mage::getModel('ecentru/ecentru');
        }
        return $this->_model;
    }

    /**
     *
     */
    public function redirectAction() {
        //очищаем корзину
        foreach (Mage::getSingleton('checkout/session')->getQuote()->getItemsCollection() as $item) {
            Mage::getSingleton('checkout/cart')->removeItem($item->getId())->save();
        }
        try {
            $url = $this->getModel()->getConfig()->getPaymentUrl();
            $fields = $this->getModel()->prepareRequestForm();
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_redirect(Mage::getUrl('checkout/onepage/failure'));
        }

        $form = new Varien_Data_Form();
        $form->setAction($url)
            ->setId('payment_form_id')
            ->setName('payment_name')
            ->setMethod('POST')
            ->setUseContainer(true);


        foreach ($fields as $field => $value) {
            $form->addField($field, 'hidden', array('name' => $field, 'value' => $value));
        }

        $form->addField("do_redirect", 'submit', array('name' => "value", 'value' => "redirect"));

        $html = '<!DOCTYPE html><html><body>';
        $html .= $this->__('You will be redirected to Payment page in a few seconds.');
        $html .= $form->toHtml();
        $html .= '<script type="text/javascript">document.getElementById(\'payment_form_id\').submit();</script>';
        $html .= '</body></html>';
        echo $html;


    }

    /**
     * <pre>
     * Receive POST with callback from payment provider
     * POST contains:
     * OrderID                               Order ID as supplied by merchant
     * OrderTotal         Order amount
     * OrderCurrency Currency used
     * CardNumber     Card number in NNNNNN**NNNN format
     * Cardholder         Cardholder name
     * TransactionID    Transaction ID internal to the system
     * Action                  Which action is being reported - make sure it is "Authorization"
     * Result                   Action result - can be "Success" or "Failure"
     * ResponseCode Bank response code ( http://www.eway.com.au/images/ResponseCodes.pdf )
     * Message                             Response code translated to message
     * </pre>
     */
    public function callbackAction() {
        $post = $this->getRequest()->getPost();

        if (!$post) {
            $this->getModel()->getHelper()->log("Invalid callback request from " . Mage::helper('core/http')->getRemoteAddr() . "; no post data");
            $this->getResponse()->setHeader('HTTP/1.1', "401", true);
            exit();
        }
        $fields = array('OrderID', 'OrderTotal', 'OrderCurrency', 'CardNumber', 'Cardholder',
            'TransactionID', 'Action', 'Result', 'ResponseCode', 'Message');
        foreach ($fields as $f) {
            if (!isset($post[$f])) {
                $this->getResponse()->setHeader('HTTP/1.1', 401, true);
                $this->getModel()->getHelper()->log("Invalid callback request from " . Mage::helper('core/http')->getRemoteAddr() . "; no " . $f . " set");
                exit();
            }
        }
        //add payment provider ip validation
        //217.12.121.219 ?
        if (Mage::helper('core/http')->getRemoteAddr() != '217.12.121.219') {
            $this->getResponse()->setHeader('HTTP/1.1', 401, true);
            $this->getModel()->getHelper()->log("Request from unknown address: " . Mage::helper('core/http')->getRemoteAddr());
            exit();
        }

        try {
            $this->getModel()->handleCallback($post);
        } catch (Exception $e) {
            $this->getModel()->getHelper()->log("Error while handling callback: " . $e->getMessage());
            Mage::logException($e);
            $this->getResponse()->setHeader('HTTP/1.1', 500, true);
        }


    }
}
