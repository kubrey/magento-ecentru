<?php

/**
 * Created by PhpStorm.
 * User: kubrey
 * Date: 27.08.15
 * Time: 10:01
 */
class Kubrey_Ecentru_InfoController extends Mage_Core_Controller_Front_Action
{

    /**
     *  terms and conditions page
     */
    public function conditionsAction() {
        $this->loadLayout();
        $block = $this->getLayout()
            ->createBlock('ecentru/info_conditions');
        $this->getLayout()->getBlock('content')->append($block);
        $this->getLayout()->getBlock('head')->setTitle($this->_('Terms and Conditions'));

        $this->renderLayout();
    }

    /**
     * privacy policy page
     */
    public function privacyAction() {
        $this->loadLayout();
        $block = $this->getLayout()->
        createBlock('ecentru/info_privacy');
        $this->getLayout()->getBlock('content')->append($block);
        $this->getLayout()->getBlock('head')->setTitle($this->__('Privacy Policy'));


        $this->renderLayout();
    }

    public function doneAction(){
          Mage::getSingleton('checkout/session')->addSuccess($this->__('Your order will be handled by the payment provider as soon  as possible'));
          $this->_redirect('checkout/cart');
    }
}
