<?php

/**
 * Created by PhpStorm.
 * User: kubrey
 * Date: 27.08.15
 * Time: 14:42
 */
class Kubrey_Ecentru_Model_Config extends Mage_Payment_Model_Method_Abstract
{

    /**
     *
     * Url to which customer will be redirected after making a
     * payment.
     *
     * This element allows specifying individual return URL for each
     * order. IF the URL is not specified, the system will use return
     * URL specified in the merchant configuration. If that URL is also
     * undefined, customer will not be returned to the merchant web site.
     *
     * NOTE! Merchant should NOT make any assumptions about status
     * of the payment (authorized/declined) from the fact that customer
     * was redirected to specified URL. Instead, merchant should rely
     * on the payment callback notifications to learn status of
     * payment.
     * @return string
     */
    public function getReturnUrl() {
        return Mage::getUrl("ecentru/info/done");
    }

    /**
     * @return string
     */
    public function getChannelName() {
        return "Polsky.TV Channel&IPTV";
    }

    public function getPaymentUrl() {
        return 'https://secure.e-centru.com/payment/payment.aspx';
    }

    /**
     * Url receiving POST callback from payment provider
     *
     * @return string
     */
    public function getNotificationUrl() {
        return Mage::getUrl("ecentru/gateway/callback", array('_secure' => true));
    }
}