<?php

/**
 * Created by PhpStorm.
 * User: kubrey
 * Date: 27.08.15
 * Time: 9:59
 */

/**
 * Class Kubrey_Ecentru_Model_Ecentru
 * @property DOMDocument $_xml
 */
class Kubrey_Ecentru_Model_Ecentru extends Mage_Payment_Model_Method_Abstract
{
    protected $_code = 'ecentru';
    protected $_xml;
    protected $_order = null;
    protected $_orderId;
    protected $_post;
    //
    protected $_config = null;
    //
    protected $_helper = null;

    //для вывода описания платежного метода
    protected $_formBlockType = 'ecentru/payment_info';


    /**
     * @return \Kubrey_Ecentru_Helper_Data
     */
    public function getHelper() {
        if (!$this->_helper) {
            $this->_helper = Mage::helper('ecentru');
        }
        return $this->_helper;
    }

    /**
     * @return \Kubrey_Ecentru_Model_Config
     */
    public function getConfig() {
        if (!$this->_config) {
            $this->_config = Mage::getModel('ecentru/config');
        }
        return $this->_config;
    }

    /**
     * Ссылка для редиректа на страницу платежки после чекаута
     * @return string
     */
    public function getOrderPlaceRedirectUrl() {
        return Mage::getUrl('ecentru/gateway/redirect', array('_secure' => true));
    }

    /**
     * This method should be called from the redirect page
     * @see $this->getOrderPlaceRedirectUrl
     * and prepare request data for payment
     * last order data is available here
     * @return array
     */
    public function prepareRequestForm() {
        $this->getPaymentData();
        $xmlData = $this->createRequestXml();
        $formFields = array(
            'requestSettings' => $xmlData['settings'],
            'requestOrder' => $xmlData['order'],
            'requestBilling' => $xmlData['billing'],
        );
        if ($xmlData['shipping']) {
            $formFields['requestShipping'] = $xmlData['shipping'];
        }

        return $formFields;

    }

    /**
     * @return null|Varien_Object
     * @throws Mage_Checkout_Exception
     */
    protected function getPaymentData() {
        $session = Mage::getSingleton('checkout/session');
        $quoteId = $session->getQuoteId();

        $order = Mage::getModel('sales/order')->loadByIncrementId($session->getLastRealOrderId());
        if (!$order->getId()) {
            throw new Mage_Checkout_Exception("Error occurred");
        }
        $this->_order = $order;
        $this->_orderId = $order->getId();

        return $this->_order;
    }

    /**
     * Для запроса можно вставить продукты в виде
     *  <Product>
     * <ID>ID#12213</ID>
     * <Name>Sausage</Name>
     * <Description>Sausage</Description>
     * <Price>1.50</Price>
     * <Quantity>1</Quantity>
     * </Product>
     * @return array
     */
    protected function getProductsInfo() {
        if (!$this->_order) {
            return array();
        }
        $products = array();
        $items = $this->_order->getAllVisibleItems();
        foreach ($items as $item) {
            $products[] = array(
                'ID' => $item->getProductId(),
                'Name' => htmlspecialchars($item->getData('name')),
                'Description' => htmlspecialchars($item->getData('description')),
                'Price' => $item->getData('price'),
                'Quantity' => $item->getData('qty_ordered')
            );
        }
        return $products;
    }

    /**
     * If has non-virtual item - needs shipping data
     * <S_Name>John</S_Name>
     * <S_LastName>Smith</S_LastName>
     * <S_Address>Madison 213</S_Address>
     * <S_Country>USA</S_Country>
     * <S_City>New York</S_City>
     * <S_State></S_State>
     * <S_ZipCode>10018</S_ZipCode>
     * <S_Mail>johnsmith@domain.com</S_Mail>
     * <S_Phone>+1 555 555-5555</S_Phone>
     * @throws Mage_Payment_Exception
     * @return array
     */
    protected function getAddressInfo($type) {
        if (!$this->_order) {
            return array();
        }
        $address = array();

        switch ($type) {
            case 'shipping':
                $data = $this->_order->getShippingAddress();
                $letter = 'S';
                break;
            case 'billing':
                $data = $this->_order->getBillingAddress();
                $letter = 'B';
                break;
            default:
                throw new Mage_Payment_Exception("Invalid address type");
        }

        $country = Mage::getModel('directory/country')->loadByCode($data->getData('country_id'));

        $address[$letter . '_Name'] = htmlspecialchars($data->getData('firstname'));
        $address[$letter . '_LastName'] = htmlspecialchars($data->getData('lastname'));
        $address[$letter . '_Address'] = htmlspecialchars($data->getData('street'));
        $address[$letter . '_Country'] = htmlspecialchars($country->getName());//get full name
        $address[$letter . '_City'] = htmlspecialchars($data->getData('city'));
        $address[$letter . '_State'] = htmlspecialchars($data->getData('region'));
        $address[$letter . '_ZipCode'] = htmlspecialchars($data->getData('postcode'));
        $address[$letter . '_Mail'] = htmlspecialchars($this->_order->getData('customer_email'));
        $address[$letter . '_Phone'] = htmlspecialchars($data->getData('telephone'));

        return $address;
    }

    /**
     * @throws Mage_Payment_Exception
     * @return array
     */
    protected function createRequestXml() {
        $this->_xml = new DomDocument('1.0', 'utf-8');

        $request = $this->_xml->appendChild($this->_xml->createElement('requestMessage'));

        //settings block
        $settingsBlock = $request->appendChild($this->_xml->createElement('requestSettings'));
        $channel = $settingsBlock->appendChild($this->_xml->createElement('ChannelName'));
        $channel->appendChild($this->_xml->createTextNode($this->getConfig()->getChannelName()));
        $returnUrl = $settingsBlock->appendChild($this->_xml->createElement('UrlToReturn'));
        $returnUrl->appendChild($this->_xml->createTextNode($this->getConfig()->getReturnUrl()));

        //order block
        $orderBlock = $request->appendChild($this->_xml->createElement('requestOrder'));
        $orderNum = $orderBlock->appendChild($this->_xml->createElement('OrderID'));
        $orderNum->appendChild($this->_xml->createTextNode($this->_orderId));
        $desc = $orderBlock->appendChild($this->_xml->createElement('Descripton'));
        $desc->appendChild($this->_xml->createTextNode($this->_order->getData('increment_id') . "-" . $this->_order->getData('grand_total')));
        //order-products subblock
        $prods = $this->getProductsInfo();
        foreach ($prods as $pr) {
            $product = $orderBlock->appendChild($this->_xml->createElement('Product'));
            foreach ($pr as $key => $prodval) {
                $attr = $product->appendChild($this->_xml->createElement($key));
                $attr->appendChild($this->_xml->createTextNode($prodval));
            }
        }
        //order block further data
        $orderTotal = $orderBlock->appendChild($this->_xml->createElement('OrderTotal'));
        $orderTotal->appendChild($this->_xml->createTextNode($this->_order->getData('grand_total')));
        $curr = $orderBlock->appendChild($this->_xml->createElement('Currency'));
        $curr->appendChild($this->_xml->createTextNode($this->_order->getData('order_currency_code')));

        // shipping block - if has hardware items
        $sh = 0;
        if ($this->_order->getData('is_virtual') == "0") {
            $shipping = $this->getAddressInfo('shipping');
            $sh = $request->appendChild($this->_xml->createElement('requestShipping'));
            foreach ($shipping as $key => $shval) {
                $attr = $sh->appendChild($this->_xml->createElement($key));
                $attr->appendChild($this->_xml->createTextNode($shval));
            }
        }

        //billing block
        $billing = $this->getAddressInfo('billing');
        $bill = $request->appendChild($this->_xml->createElement('requestBilling'));
        foreach ($billing as $key => $billval) {
            $attr = $bill->appendChild($this->_xml->createElement($key));
            $attr->appendChild($this->_xml->createTextNode($billval));
        }
        $this->getHelper()->log($this->_xml->saveXML());

        return array(
            'full' => $this->_xml->saveXML(),
            'settings' => $this->_xml->saveXML($settingsBlock),
            'order' => $this->_xml->saveXML($orderBlock),
            'shipping' => (($sh) ? $this->_xml->saveXML($sh) : null),
            'billing' => $this->_xml->saveXML($bill),
        );
    }

    /**
     * Handling payment callback after checkout
     * @param array $post
     * @throws Exception
     * @return null|bool null - если не было обработки по нормальным причинам
     */
    public function handleCallback($post) {
        //check if action is Authorization
        if ($post['Action'] != 'Authorization') {
            //doing nothing, supposed to be another event from payment-provider
            return null;
        }

        $orderModel = Mage::getModel("sales/order");
        $order = $orderModel->load($post['OrderID']);
        if (!$order->getId()) {
            throw new Exception("Failed to get order by order " . $post['OrderID']);
        }
        $this->_order = $order;
        $this->_orderId = $order->getId();

        //check order amount and currency
        $orderTotal = $order->getData('grand_total');
        $orderCurr = $order->getData('order_currency_code');
        if ($orderCurr != $post['OrderCurrency']) {
            throw new Exception("Wrong currency code " . $post['OrderCurrency'] . " for orderId " . $this->_orderId);
        }

        if ($orderTotal != $post['OrderTotal']) {
            throw new Exception("Wrong amount " . $post['OrderTotal'] . " for orderId " . $this->_orderId);
        }

        //check if this order was not set as paid before
        //to prevent doubling callbacks
        $payment = $order->getPayment();
        if ($payment->getMethod() != $this->getCode()) {
            throw new Exception("Order " . $this->_orderId . " has another payment method already");
        }
        if ($payment->getData('amount_paid') > 0 && $payment->getData('last_trans_id')) {
            throw new Exception("Order " . $this->_orderId . " has already been payed");
        }
        $this->_post = $post;

        switch ($post['Result']) {
            case 'Success':
                $this->registerPaymentCapture();
                break;
            case 'Failure':
                $this->registerPaymentCanceled();
                break;
            default:
                throw new Exception("Invalid Result value: " . $post['Result']);

        }

        return true;
    }

    /**
     *
     * @return \Kubrey_Ecentru_Model_Ecentru
     */
    private function registerPaymentCapture() {
        $payment = $this->_order->getPayment();
        $idTrans = $this->_post['TransactionID'];
        $payment->setTransactionId($idTrans)
            ->setParentTransactionId($this->_order->getData('customer_email'))
            ->setShouldCloseParentTransaction('ok')
            ->setIsTransactionClosed(0)
            ->registerCaptureNotification($this->_order->getGrandTotal());
        $payment->setData('additional_data', $this->_post['Message']);
        $payment->setData('cc_owner', $this->_post['Cardholder']);

        $this->_order->setData('state', Mage_Sales_Model_Order::STATE_COMPLETE);
        $this->_order->setStatus(Mage_Sales_Model_Order::STATE_COMPLETE);
        $this->_order->save();

        if ($invoice = $payment->getCreatedInvoice()) {
            $this->_order->addStatusHistoryComment(
                Mage::helper('backoffice')->__('Notified customer about invoice #%s.', $invoice->getIncrementId())
            )
                ->setIsCustomerNotified(true)
                ->save();
        }
        return $this;
    }

    /**
     *
     * @return \Kubrey_Ecentru_Model_Ecentru
     */
    private function registerPaymentCanceled() {
        $this->getHelper()->log($this->_orderId . " status=>canceled");
        $payment = $this->_order->getPayment();
        if ($this->_order->canCancel()) {
            $this->_order->cancel();
            $payment->setData('additional_data', $this->_post['Message']);
            $this->_order->addStatusToHistory(Mage_Sales_Model_Order::STATE_CANCELED, "");
            $this->_order->save();
        } else {
            $this->getHelper()->log($this->_order->getId() . " failed to be canceled", Zend_Log::WARN);
        }
        $this->_order->save();

        return $this;
    }
}